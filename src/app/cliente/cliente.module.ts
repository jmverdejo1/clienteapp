import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClienteFormComponent } from './cliente-form/cliente-form.component';
import { ClienteMainComponent } from './cliente-main/cliente-main.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { ClienteDetailComponent } from './cliente-detail/cliente-detail.component';
import { ProductoMainComponent } from '../producto/producto-main/producto-main.component';



@NgModule({
  declarations: [
    ClienteFormComponent,
    ClienteMainComponent,
    ClienteDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AppRoutingModule
  ],
  exports:[
    ClienteFormComponent,
    ClienteMainComponent,
    FormsModule
  ]

})
export class ClienteModule { }
