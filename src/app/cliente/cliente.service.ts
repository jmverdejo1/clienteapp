import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioService } from '../usuario/usuario.service';
import { Cliente } from './interfaces/cliente';
import { Region } from './interfaces/region';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private urlBase:string="http://localhost:8087/api/clientes";

  constructor(private http:HttpClient, private servicioUsuario:UsuarioService) { }

  httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  agregarAuthorizationHeader(){
    let token = this.servicioUsuario.token;
    if(token != null){
      return this.httpHeaders.append('Authorization','Bearer '+token);
    }

    return this.httpHeaders;
  }

  mostrarCliente():Observable<Cliente[]>{
    const url = this.urlBase;
    return this.http.get<Cliente[]>(url);
  }

  //metodo para enviar datos a api post
  guardarCliente(cliente:Cliente):Observable<Cliente>{
    const url = this.urlBase;
    return this.http.post<Cliente>(url, cliente,{headers: this.agregarAuthorizationHeader()});
  }

  //buscar cliente por id
  getCliente(id:number):Observable<Cliente>{
    return this.http.get<Cliente>(`${this.urlBase}/${id}`,{headers: this.agregarAuthorizationHeader() });
  }
  //actualizar cliente
  update(cliente:Cliente):Observable<Cliente>{
    return this.http.put<Cliente>(`${this.urlBase}/${cliente.id}`, cliente,{headers: this.agregarAuthorizationHeader() });
  }
  //eliminar cliente
  delete(id:number):Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlBase}/${id}`,{headers: this.agregarAuthorizationHeader() });
  }

  getRegiones():Observable<Region[]>{
    return this.http.get<Region[]>(`${this.urlBase}/regiones`,{headers: this.agregarAuthorizationHeader() });
  }

  subirImagen(archivo:File, id:any):Observable<HttpEvent<any>>{
    let formData = new FormData();
    formData.append("archivo",archivo);
    formData.append("id",id);
    let httpHeaders = new HttpHeaders();
    let token = this.servicioUsuario.token;
    if(token!= null){
      httpHeaders = httpHeaders.append('Authorization','Bearer '+token);
    }
    const req = new HttpRequest('POST',`${this.urlBase}/uploads` ,formData, {headers: httpHeaders});

    return this.http.request(req).pipe(
      resp => resp
    );
  }

}
