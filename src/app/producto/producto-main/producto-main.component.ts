import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/usuario/usuario.service';
import swal from 'sweetalert2';
import { Producto } from '../interfaces/producto';
import { ProductoService } from '../producto.service';

@Component({
  selector: 'app-producto-main',
  templateUrl: './producto-main.component.html',
  styleUrls: ['./producto-main.component.css']
})
export class ProductoMainComponent implements OnInit {
  productos:Producto[] = [];
  constructor(private servicio:ProductoService, public servicioUsuario:UsuarioService) { }

  httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  ngOnInit(): void {
    this.servicio.mostrarProductos().subscribe(
      resp => {this.productos = resp;console.log("Respuesta: ",this.productos)}
    );
  }

  borrarProducto(producto: Producto) {
    swal({
      title:"Está seguro?",
      text:`Seguro que desea eliminar el producto ${producto.nombre}}`,
      type:'warning',
      showCancelButton:true,
      confirmButtonColor:'#3085d6',
      cancelButtonColor:'#d33',
      confirmButtonText:'Si, eliminar!',
      cancelButtonText:'No, cancelar',
      confirmButtonClass:'btn btn-info',
      cancelButtonClass:'btn btn-danger',
      buttonsStyling:false,
      reverseButtons:true
    }).then((result)=>{
      if(result.value){
        this.servicio.deleteProducto(producto.id).subscribe(
          resp => {this.productos = this.productos.filter( cli => cli !== producto)
            swal('Producto eliminado',`El producto ${producto.nombre} ha sido eliminado con éxito`,'success');
            },
          error => {
            swal('Error',`Cliente ${producto.nombre} no ha sido borrado: ${error.status}`,'error');
          }
        )
      }
    });
  }
}
