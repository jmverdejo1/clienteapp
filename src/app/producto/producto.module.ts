import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductoMainComponent } from './producto-main/producto-main.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { RouterModule } from '@angular/router';
import { ProductoFormComponent } from './producto-form/producto-form.component';



@NgModule({
  declarations: [ProductoMainComponent, ProductoFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    AppRoutingModule
  ],
  exports:[
    ProductoMainComponent,
    FormsModule
  ]
})
export class ProductoModule { }
