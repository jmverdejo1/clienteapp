import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioService } from '../usuario/usuario.service';
import { Producto } from './interfaces/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  private urlBase:string="http://localhost:8087/api/productos";

  httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http:HttpClient, private servicioUsuario:UsuarioService) { }

  agregarAuthorizationHeader(){
    let token = this.servicioUsuario.token;
    if(token != null){
      return this.httpHeaders.append('Authorization','Bearer '+token);
    }

    return this.httpHeaders;
  }

  mostrarProductos():Observable<Producto[]>{
    const url = this.urlBase;
    return this.http.get<Producto[]>(url);
  }

  guardarProducto(producto:Producto):Observable<Producto>{
    const url = this.urlBase;
    return this.http.post<Producto>(url, producto,{headers: this.agregarAuthorizationHeader()});
  }

  //buscar cliente por id
  getProducto(id:number):Observable<Producto>{
    return this.http.get<Producto>(`${this.urlBase}/${id}`,{headers: this.agregarAuthorizationHeader() });
  }
  //actualizar cliente
  updateProducto(producto:Producto):Observable<Producto>{
    return this.http.put<Producto>(`${this.urlBase}/${producto.id}`, producto,{headers: this.agregarAuthorizationHeader() });
  }
  //eliminar cliente
  deleteProducto(id:number):Observable<Producto>{
    return this.http.delete<Producto>(`${this.urlBase}/${id}`,{headers: this.agregarAuthorizationHeader() });
  }
}
