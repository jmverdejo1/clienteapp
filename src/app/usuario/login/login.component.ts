import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Usuario } from '../clases/usuario';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor(private servicio:UsuarioService, private router:Router) { }

  usuario:Usuario={
    username: '',
    password: '',
    enabled: false,
    roles: [],
    nombre: '',
    apellido: '',
    email: ''
  };

  ngOnInit(): void {
    if(this.servicio.token){
      swal("Aviso","Ya estas autenticado!","info");
      this.router.navigate(['']);
    }
  }

  login():void{
    this.servicio.login(this.usuario).subscribe(
      resp => {console.log("respuesta:",resp.access_token),
      this.servicio.guardarToken(resp.access_token),
      this.servicio.guardarUsuario(resp.access_token),
      swal('Login',`Hola ${this.usuario.username}, ha iniciado sesión con exito`,'success'),
      this.router.navigate(['']);
    },
      error => {console.log("error",error);
      swal('Error Login',`Error: ${error.status}`,'error');
    }
    );
  }

}
