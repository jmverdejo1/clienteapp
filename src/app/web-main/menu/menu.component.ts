import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/usuario/usuario.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  user_name!:string;

  constructor(public servicio:UsuarioService,private router:Router) { }

  cerrarSesion():void{
    this.user_name = this.servicio.usuario.username;
    this.servicio.logout();
    swal('Logout',`${this.user_name}, has cerrado sesión correctamente`, 'success')
    this.router.navigate(['/login']);
    console.log("cerrarSesion")
  }

  ngOnInit(): void {
  }

}
